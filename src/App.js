import React,{Component} from 'react';
import './App.css';
import {Container, Row, Col, Button} from 'reactstrap';
import ApiComponent from './components/apiComponent';

class App extends Component{
  constructor(props) {
      super(props);
      this.state = {
          showApiComponent: false
      }
    }  

    _toggleApiComponent(){
      const { showApiComponent } = this.state;
      this.setState({
        showApiComponent: !showApiComponent
      })
    }
    render(){
      const { showApiComponent } = this.state;
      let buttonValue =  '';
      if(showApiComponent){
        buttonValue = 'Hide Component'
      }else {
        buttonValue = 'Show Component'
      }
      return (
    <Container style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}>
      <Row>
      <Button onClick={()=>{this._toggleApiComponent()}} style={{padding: '10px', backgroundColor: 'skyblue', color: 'white'}}>{buttonValue}</Button>
        {showApiComponent ?<ApiComponent/>: null}
      </Row>
    </Container>
  );
    }
}

export default App;
