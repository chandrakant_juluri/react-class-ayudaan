import { title } from 'process';
import React,{Component} from 'react';
import {Button, Card, CardBody, Col, Row, Modal, ModalBody, Input, ModalHeader, InputGroup, InputGroupAddon, Label} from 'reactstrap';

class ApiComponent extends Component{
    constructor(props) {
        super(props);
        this.state = {
            content: [],
            displayAddContentModal: false,
            addContentTitle: '',
            editId: ''
        }
      }

      _toggleAddContentModal(){
        const {displayAddContentModal} = this.state;
        this.setState({
            displayAddContentModal: !displayAddContentModal
        })
      }
      
    render(){
        const { content, displayAddContentModal, editId } = this.state;
        return  (
        <div>
            <Card>
                <CardBody>
                    <Row>
                        <Row>
                        {!displayAddContentModal ? <Button style={{padding: '10px', backgroundColor: 'skyblue', color: 'white'}} onClick={() => {this._toggleAddContentModal()}}>Add Content</Button> : null}
                            {displayAddContentModal ? (
                                <Row>
                                    <Input style={{padding: '10px'}} placeholder="Title" onChange={(e) => this.setState({addContentTitle: e.target.value})}></Input>
                                    <Button style={{padding: '10px', backgroundColor: 'skyblue', color: 'white'}} onClick={()=> {this._submitNewContent()}}>Submit</Button>
                                </Row>
                            ): null}
                        </Row>
                <Row>
                    <ol>
                    {content.map(c =><li style={{padding: '10px'}}> <Col lg="6">
                        <Label>{c.title}</Label>{' '}
                        <Button onClick={()=> this.setState({
                            editId: c.id
                        })}>Edit</Button>
                        <br/>
                        {editId === c.id ? (
                            <Row>
                            <Input style={{padding: '10px', margin: '10px'}} placeholder="Title" onChange={(e) => this.setState({addContentTitle: e.target.value})}></Input>
                            <Button onClick={()=>{this._submitContentEdit()}}>Submit</Button>
                            <Button onClick={()=> this.setState({
                            editId: ''
                        })}>Cancel</Button>
                            </Row>
                        ):null}
                        
                        </Col> </li>)}
                    </ol>

                </Row>
                
                </Row>
                </CardBody>
            </Card>
            {/* {this._renderAddContentModal()} */}
            </div>
        )

    }

    // _renderAddContentModal(){
    //     const {displayAddContentModal} = this.state;
    //     return(
    //         <Modal isOpen={displayAddContentModal} toggle={()=> this._toggleAddContentModal()} className="modal-primary"
    //         size="lg"
    //         centered={true}
    //         zIndex="500">
    //             <ModalHeader toggle={() => this._toggleAddContentModal()}>
    //       Add Content
    //     </ModalHeader>
    //             <ModalBody>
    //                 <Row>
    //                     <Col>
    //                         <Input placeholder="Title"></Input>
    //                     </Col>
    //                 </Row>
    //             </ModalBody>
    //         </Modal>
    //     )
    // }

    _submitContentEdit(){
        const {editId, addContentTitle, content} = this.state;
        fetch(`https://jsonplaceholder.typicode.com/posts/${editId}`, {
  method: 'PUT',
  body: JSON.stringify({
    id: editId,
    title: addContentTitle,
    body: 'bar',
    userId: editId,
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
})
  .then((response) => response.json())
  .then((json) => {
    content.forEach(c => {
        if(c.id === editId){
            c.title = addContentTitle
        }
    })
    this.setState({
        content: content,
        editId: ''
    })
  });
    }

    _submitNewContent(){
        const {addContentTitle, content} = this.state;
        fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST',
  body: JSON.stringify({
    title: addContentTitle,
    body: 'bar',
    userId: 1,
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
})
  .then((response) => response.json())
  .then((json) => this.setState({
      content: [json, ...content],
      displayAddContentModal: false
  }));
    }

    componentDidMount(){
        const { content } = this.state;
        if(content.length === 0){
            fetch('https://jsonplaceholder.typicode.com/posts')
        .then((response) => response.json())
        .then(response =>
            this.setState({
                content: response
            })
        );
        }
        }

        componentWillUnmount() {
            console.log('API component unmounting!!!')
        }
}

export default ApiComponent;